-- mysql_setup.sql
-- ==========================================================================
-- bootstrap the mysql database for use
-- ==========================================================================
drop database if exists product_coding_challenge;
create database product_coding_challenge;

-- don't do this at home kiddos ;)
grant all privileges on product_coding_challenge.* to code_challenge@localhost identified by 'code_challenge';
