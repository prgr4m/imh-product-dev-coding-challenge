<?php

class BGColor {
    public static function on_activate() {}

    public static function on_deactivate() {
        unregister_setting('general', 'bgcolor');
    }

    public static function admin_settings_init() {
        register_setting(
            'general',
            'bgcolor',
            [
                'type' => 'string',
                'description' => 'customizable background color for website',
                'sanitize_callback' => 'sanitize_hex_color',
                'show_in_rest' => true,
                'default' => '#d1e4dd'
            ]
        );

        add_settings_field(
            'bgcolor',
            'Background Color',
            array('BGColor', 'admin_color_picker_callback'),
            'general'
        );
    }

    public static function admin_color_picker_callback($args) {
        $bgcolor = get_option('bgcolor', '#d1e4dd');
?>
        <input id="bgcolor" name="bgcolor" type="color" value="<?php echo $bgcolor; ?>"/>
<?php
    }

    public static function apply_bgcolor() {
        $bgcolor = get_option('bgcolor', '#die4dd');
?>
    <style> 
        body { 
            background-color: <?php echo $bgcolor; ?>
        }
    </style> 
<?php
    } // end of apply_bgcolor
} // end of class
