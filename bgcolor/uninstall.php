<?php
/**
 * Uninstall Script
 */
if (!defined('WP_UNINSTALL_PLUGIN')) {
    die; // script called directly and not running under wp env
}

$opt_name = 'bgcolor';
delete_option($opt_name);
