<?php
/**
 * Plugin Name:       BGColor
 * Plugin URI:        https://gitlab.com/prgr4m/imh-product-dev-coding-challenge
 * Description:       A color picker plugin to change the background color
 * Version:           1.0.0
 * Requires at least: 5.6.1
 * Requires PHP:      7.4.15
 * Author:            John Boisselle
 * Author URI:        https://johnboisselle.com
 * License:           WTDHPL - Trying to keep it clean and pro ;)
 * License URI:       http://wtdhpl.info
 */
define('DIR_SEP', DIRECTORY_SEPARATOR);
define('BGCOLOR_PLUGIN_DIR', plugin_dir_path(__FILE__));
require_once BGCOLOR_PLUGIN_DIR . DIR_SEP . 'class.bgcolor.php';

register_activation_hook(__FILE__, array('BGColor', 'on_activate'));
register_deactivation_hook(__FILE__, array('BGColor', 'on_deactivate'));

add_action('admin_init', array('BGColor', 'admin_settings_init'));
add_action('wp_head', array('BGColor', 'apply_bgcolor'));
