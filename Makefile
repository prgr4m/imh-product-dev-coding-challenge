MYSQL_ADMIN_USER ?= $(shell read -p "mysql admin user: " admin_user; echo $$admin_user)

install: clean db_setup
	wget https://wordpress.org/latest.tar.gz
	tar xzf latest.tar.gz
	rm -f latest.tar.gz
	mv wordpress www
	git clone https://gitlab.com/prgr4m/imh-product-dev-coding-challenge.git bgcolor_root
	mv bgcolor_root/bgcolor www/wp-content/plugins/bgcolor
	rm -rf bgcolor_root
	cp data/wp-config.php www/wp-config.php
	cp data/.htaccess www/.htaccess
	cp data/phpinfo.php www/phpinfo.php

clean:
	rm -rf bgcolor_root
	rm -rf www
	rm -f latest.tar.gz

db_setup:
	mysql -u $(MYSQL_ADMIN_USER) -p < data/mysql_setup.sql

local_copy:
	rm -rf www/wp-content/plugins/bgcolor
	cp -r bgcolor www/wp-content/plugins/bgcolor
